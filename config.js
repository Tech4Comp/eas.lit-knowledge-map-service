'use strict';

const { isEmpty } = require('lodash');

const domain = (!isEmpty(process.env.VIRTUAL_HOST)) ? process.env.VIRTUAL_HOST : 'localhost';
const port = (!isEmpty(process.env.VIRTUAL_PORT)) ? process.env.VIRTUAL_PORT : '3000';
const protocol = 'http';
const hostname = protocol + '://' + domain + ((port === '80') ? '' : ':' + port);
console.info('Using ' + hostname + ' as hostname');

const sparqlEndpoint = (!isEmpty(process.env.SERVICE_URL_SPARQL)) ? process.env.SERVICE_URL_SPARQL : 'http://localhost:8080/eal';
console.info('Using ' + sparqlEndpoint + ' as sparql endpoint');

const dataGraphName = (!isEmpty(process.env.DATA_GRAPH_NAME)) ? process.env.DATA_GRAPH_NAME : hostname + '/data';
console.info('Using ' + dataGraphName + ' as graph name for data');

const counterGraphName = (!isEmpty(process.env.COUNTERS_GRAPH_NAME)) ? process.env.COUNTERS_GRAPH_NAME : hostname + '/counters';
console.info('Using ' + counterGraphName + ' as graph name for counters');

const counterName = hostname + '/counter';

module.exports = {
	domain: domain,
	port: port,
	protocol: protocol,
	hostname: hostname,
	dataGraphName: dataGraphName,
	sparqlEndpoint: sparqlEndpoint,
	counterGraphName: counterGraphName,
	counterName: counterName
};

FROM node:lts-alpine
LABEL maintainer="Roy Meissner <roy.meissner@uni-leipzig.de>"

RUN apk add --no-cache python3 make build-base git raptor2 && rm -rf /var/cache/apk/*

ARG BUILD_ENV=LOCAL
ENV APPLICATION_PORT=${APPLICATION_PORT:-80}
RUN mkdir /nodeApp
WORKDIR /nodeApp

COPY ./ ./
RUN if [ "$BUILD_ENV" = "CI" ] ; then npm prune --production ; else rm -R node_modules ; npm install --production ; fi

CMD ["npm","start"]

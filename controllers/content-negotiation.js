'use strict';

const boom = require('@hapi/boom'),
	sparql = require('../sparql'),
	{ IsValidJson } = require('../common'),
	{ isEmpty } = require('lodash');

module.exports = {
	write: async function(request, h) {
		try {
			let data = request.payload;

			switch (request.headers['content-type']) {
				case 'application/ld+json':
					if(IsValidJson(data)) {
						if(isEmpty(data['@context']))
							return boom.badData('Data is not valid JSON-LD data');
						else {
							let resultingID = undefined;
							if(!isEmpty(data['@id']))
								resultingID = await sparql.write(data);
							else
								resultingID = await sparql.create(data);
							console.log(resultingID);
							return internalGet(request, h, resultingID);
						}
					} else
						return boom.badData('Data is not valid JSON data');
				default:
					return boom.unsupportedMediaType();
			}
		} catch (error) {
			if (error instanceof Object && error['@type'] === 'ValidationReport')
				return boom.badData('Bad data, see report' , error);
			console.error(error);
			request.log('error', error);
			return boom.badImplementation();
		}
	},

	read: async function(request, h, customQuery = undefined) {
		try {
			let subject = request.url.href;
			let toRespond;
			try {
				toRespond = await sparql.read(subject, customQuery);
			} catch (e) {
				return boom.notFound();
			}

			const accepts = request.headers.accept.split(',');
			const acceptAll = accepts.find((accept) => accept.includes('*/*'));
			if(!isEmpty(acceptAll))
				accepts[0] = 'application/json';
			switch (accepts[0]) {
				case 'application/json':
				case 'application/ld+json':
					return h.response(toRespond).type('application/ld+json');
				default:
					return boom.unsupportedMediaType();
			}
		} catch(error) {
			console.error(error);
			request.log('error', error);
			return boom.badImplementation();
		}
	},
};

async function internalGet(request, h, url) {
	const response = await global.server.inject({url: url, headers: request.headers});
	return h.response(response.result).code(response.statusCode).type(response.headers['content-type']);
}

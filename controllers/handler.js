'use strict';

const { badImplementation } = require('@hapi/boom'),
	sparql = require('../sparql'),
	{ dataGraphName } = require('../config'),
	{ read } = require('./content-negotiation');

module.exports = {
	readByElement: async function(request, h) {
		const targetClass = getTargetClass(request.route.settings.tags);
		const id = request.params.id;
		const customQuery = `
			prefix eal: <http://tech4comp/eal/>
			CONSTRUCT {
				?s ?p ?o .
			} WHERE {
				GRAPH <${dataGraphName}> {
					?s a ${targetClass} ;
						eal:Element <${id}> ;
						?p ?o .
				}
			}`;
		return read(request, h, customQuery);
	},

	readByProject: async function(request, h) {
		const targetClass = getTargetClass(request.route.settings.tags);
		const projectID = request.params.id;
		const customQuery = `
			prefix eal: <http://tech4comp/eal/>
			CONSTRUCT {
				?s ?p ?o .
			} WHERE {
				GRAPH <${dataGraphName}> {
					?s a ${targetClass} ;
						eal:hasProject <${projectID}> ;
						?p ?o .
				}
			}`;
		return read(request, h, customQuery);
	},

	readKnowledgeMapLink: async function(request, h) {
		if(request.params.id === '*') {
			const targetClass = getTargetClass(request.route.settings.tags);
			const customQuery = `
				CONSTRUCT {
					?s a ${targetClass} .
				} WHERE {
				 GRAPH <${dataGraphName}> {
					?s a ${targetClass} .
				 }
			 }`;
			return read(request, h, customQuery);
		} else
			return read(request, h);
	},

	readKnowledgeMap: async function(request, h) {
		if(request.params.id === '*') {
			const targetClass = getTargetClass(request.route.settings.tags);
			const dataGraphName = 'http://triplestore.tech4comp.dbis.rwth-aachen.de/Wissenslandkarten/data/Session_Laura';
			const customQuery = `
				CONSTRUCT {
					?s a ${targetClass} .
				} WHERE {
				 GRAPH <${dataGraphName}> {
					?s a ${targetClass} .
				 }
			 }`;
			return read(request, h, customQuery);
		} else
			return await read(request, h);
	},

	delete: async function(request) {
		try {
			return await sparql.delete(request.url.href);
		} catch(error) {
			console.error(error);
			request.log('error', error);
			return badImplementation();
		}
	},
};

function getTargetClass (tags) {
	return tags.find((item) => item.startsWith('a ')).replace('a ','');
}

'use strict';

const Joi = require('joi'),
	{ badRequest } = require('@hapi/boom'),
	{ isEmpty } = require('lodash'),
	{ write } = require('./controllers/content-negotiation'),
	handler = require('./controllers/handler');

module.exports = [

	// {
	// 	method: 'Get',
	// 	path: '/knowledgeMap/{id}',
	// 	config: {
	// 		handler: handler.readKnowledgeMap,
	// 		tags: ['api', 'a <http://tech4comp/eal/knowledgeMap>'],
	// 		description: 'Get a knowledge map or a list of knowledge maps',
	// 		validate: {
	// 			params: Joi.object({
	// 				id: Joi.alternatives().try(
	// 					Joi.string().trim().uri(),
	// 					Joi.string().trim().lowercase().valid('*')
	// 				).description('Id of the knowledge map as a URI or *')
	// 			})
	// 		},
	// 		plugins: {
	// 			'hapi-swagger': {
	// 				produces: ['application/ld+json'],
	// 				responses: {
	// 					200: {},
	// 					400: {
	// 						description: 'Probably a parameter is missing or not allowed'
	// 					},
	// 					415: {
	// 						description: 'The accept header is not supported'
	// 					},
	// 					422: {
	// 						description: 'One of the data type checks failed, see message'
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// },

	{
		method: 'Get',
		path: '/knowledgeMapLink/element/{id}',
		config: {
			handler: handler.readByElement,
			tags: ['api', 'a <http://tech4comp/eal/knowledgeMapLink>'],
			description: 'Get all knowledge map links for a specific element (e.g. item)',
			validate: {
				params: Joi.object({
					id: Joi.string().trim().uri().description('Id of the element as a URI')
				})
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/ld+json'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'Get',
		path: '/knowledgeMapLink/project/{id}',
		config: {
			handler: handler.readByProject,
			tags: ['api', 'a <http://tech4comp/eal/knowledgeMapLink>'],
			description: 'Get all knowledge map links for a specific project',
			validate: {
				params: Joi.object({
					id: Joi.string().trim().uri().description('Id of the project as a URI')
				})
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/ld+json'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'GET',
		path: '/knowledgeMapLink/{id}',
		config: {
			handler: handler.readKnowledgeMapLink,
			tags: ['api', 'a <http://tech4comp/eal/knowledgeMapLink>'],
			description: 'Get a knowledge map link',
			validate: {
				params: Joi.object({
					id: Joi.string().trim().lowercase().description('Use * to request a list of all existing entities')
				})
			},
			plugins: {
				'hapi-swagger': {
					produces: ['application/ld+json'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'PUT',
		path: '/knowledgeMapLink',
		config: {
			handler: write,
			tags: ['api', 'a <http://tech4comp/eal/knowledgeMapLink>'],
			description: 'Create or update links to kowledge maps and their subjects',
			validate: {
				payload: Joi.object().keys({
					'@context': Joi.object().required(),
					'@id': Joi.string().trim().uri().example('http://item.easlit.erzw.uni-leipzig.de/knowledgeMapLink/1'),
					'@type': Joi.string().trim().uri().example('http://tech4comp/eal/knowledgeMapLink'),
					'project': Joi.string().trim().uri().example('http://item.easlit.erzw.uni-leipzig.de/project/1'),
					'element': Joi.string().trim().uri().example('http://item.easlit.erzw.uni-leipzig.de/item/1'),
					'knowledgeMap': Joi.string().trim().uri().example('http://triplestore.tech4comp.dbis.rwth-aachen.de/Wissenslandkarten/data/1'),
					'topics': Joi.array().items(
						Joi.string().trim().uri().example('http://halle/domainmodel/test'),
					).min(1)
				}).fork(['@context', '@type', 'project', 'element', 'knowledgeMap', 'topics'], (schema) => schema.required()).label('Knowledge Map Link'),
				failAction: failAction
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/ld+json'],
					produces: ['application/ld+json'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

];

function failAction(request, h, err) {
	const type = request.headers['content-type'];
	if(type !== 'application/json' && type !== 'application/ld+json')
		if(isEmpty(request.payload))
			return badRequest('No data');
		else
			return h.continue;
	else
		throw badRequest(err.output.payload.message);
}

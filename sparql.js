'use strict';

const { post } = require('got'),
	{ sparqlEndpoint, dataGraphName, counterGraphName, counterName, hostname } = require('./config'),
	{ Generator } = require('sparqljs'),
	{ quadToStringQuad } = require('rdf-string'),
	{ isEmpty } = require('lodash'),
	jsonldConverter = require('./json-processing/json-to-jsonld'),
	AwaitLock = require('await-lock').default;

let lock = new AwaitLock();

module.exports = {

	create: async function(jsonld, newElement = true) {
		if(newElement) {
			let newID = await this.getNewID();
			jsonld['@id'] = hostname + '/knowledgeMapLink/' + newID;
		}

		// TODO convert to triples
		const quads = await jsonldConverter.jsonldToQuadStrings(jsonld);
		const triples = quads.map((quad) => {
			let triple = quadToStringQuad(quad);
			delete triple.graph;
			return triple;
		});

		const query = {
			type: 'update',
			updates: [ {
				updateType: 'insert',
				insert: [{
					type: 'graph',
					triples: triples,
					name: dataGraphName
				}]
			}]
		};

		const stringQuery = (new Generator()).stringify(query);

		await post(sparqlEndpoint, {
			headers: {'Content-Type': 'application/sparql-update'},
			body: stringQuery
		});

		return jsonld['@id'];
	},

	write: async function(jsonld) {
		if(await isExisting.bind(this)(jsonld))
			await deleteExistingData.bind(this)(jsonld);
		return this.create(jsonld, false);
	},

	delete: async function(subject) {

		const query = `DELETE WHERE {
			GRAPH <${dataGraphName}> { <${subject}> ?p ?o . }
		}`;

		return post(sparqlEndpoint, {
			headers: {'Content-Type': 'application/sparql-update', 'Accept': 'application/json'},
			body: query
		});
	},

	read: async function(subject, customQuery = undefined) {
		//NOTE this function is partially copied to shacl.js
		const query = (customQuery) ? customQuery : `
			CONSTRUCT {
				<${subject}> ?p ?o .
			 }
			WHERE {
			 GRAPH <${dataGraphName}> {
				 <${subject}> ?p ?o .
			 }
			}`;

		let response = await post(sparqlEndpoint, {
			headers: {'Content-Type': 'application/sparql-query', 'Accept': 'application/ld+json'},
			body: query
		});

		response = JSON.parse(response.body);

		if(Object.keys(response).length === 0 || isEmpty(response['@id']) && isEmpty(response['@graph']) )
			throw 'response is empty';

		return response;
	},

	getNewID: async function (increment = true, name = counterName) {
		await lock.acquireAsync();
		try {
			const query = `prefix eal: <http://tech4comp/eal/>
				SELECT ?value
				WHERE {
					GRAPH <${counterGraphName}> { <${name}> <eal:count> ?value }
				}`;
			let response = await post(sparqlEndpoint, {
				headers: {'Content-Type': 'application/sparql-query', 'Accept': 'application/json'},
				body: query
			});
			response = response.body;
			if(increment) {
				const query2 = `prefix eal: <http://tech4comp/eal/>
					DELETE {
						GRAPH <${counterGraphName}> { <${name}> <eal:count> ?old }
					}
					INSERT {
						GRAPH <${counterGraphName}> { <${name}> <eal:count> ?new }
					}
					WHERE {
						GRAPH <${counterGraphName}> { <${name}> <eal:count> ?old }
						bind(?old+1 as ?new) .
					}`;
				await post(sparqlEndpoint, {
					headers: {'Content-Type': 'application/sparql-update', 'Accept': 'application/json'},
					body: query2
				});
			}
			const result = JSON.parse(response).results.bindings;
			if(isEmpty(result))
				return false;
			else
				return result[0].value.value;
		} catch (e) {
			console.error(e);
			return false;
		} finally {
			lock.release();
		}
	},

	createCounters: async function (name = counterName) {
		try {
			if ( (await this.getNewID(false)) !== false ) // NOTE skip if counter already exists
				return true;

			const query = `INSERT DATA {
				GRAPH <${counterGraphName}> { <${name}> <eal:count> 1 }
			}`;

			await post(sparqlEndpoint, {
				headers: {'Content-Type': 'application/sparql-update', 'Accept': 'application/n-triples'},
				body: query
			});

			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	},
};

async function extractSubjects(jsonld) {
	const quads = await jsonldConverter.jsonldToQuadStrings(jsonld);
	const subjects = quads.map((quad) => {
		const triple = quadToStringQuad(quad);
		if(!isBlankNode(triple.subject))
			return triple.subject;
		else
			return undefined;
	});
	return new Set(subjects.filter((subject) => subject !== undefined));
}

async function deleteExistingData(jsonld) { //TODO how to delete orphaned blank nodes
	const toDelete = await extractSubjects(jsonld);
	const existingsSubjects = Array.from(toDelete).map((subject) => this.delete(subject));
	return Promise.all(existingsSubjects);
}

async function isExisting (jsonld) {
	const subjects = await extractSubjects(jsonld);
	const existingsSubjects = Array.from(subjects).map((subject) => this.read(subject));
	// Exits if one subject does not exist
	return Promise.all(existingsSubjects).then(() => true).catch(() => false);
}

function isBlankNode (subject) {
	return subject.startsWith('_:');
}
